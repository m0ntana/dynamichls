<?php
require_once("lib.class.php");

$r = new RequestHandler();
$r->go();

if($r->type() == "dir") {
?>

<html>
<head>
    <script src="//code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
    <div id="filelist">
    <?
	foreach($r->data() as $row) {
	    echo "<a href='" . $row["url"] . "'>" . $row["name"] . "</a><br/>\n";
	}
    ?>
    </div>
</body>


</html>
<?
} // end of "dir" type block
else if($r->type() == "playlist") {
    echo $r->data();
}
?>