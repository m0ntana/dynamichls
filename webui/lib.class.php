<?php
class Config {
    private static $instance = false;
    private $FILE = "../etc/config.json";
    
    private $PARAMS = false;
    
    function __construct() {
	$data = file_get_contents($this->FILE);
	$this->PARAMS = json_decode($data, true);
    }
    
    public static function params() {
	if(!Config::$instance)
	    Config::$instance = new Config();
	return Config::$instance->PARAMS;
    }
}

class Dirs {
    function __construct() {
	$this->api = new Api();
    }
    
    function read($path = "/") {
	$rows = $this->api->request("dir/" . $path);
	foreach($rows as &$row) {
	    if($row["type"] == "dir")
		$row["url"] = "/dir/" . $row["name_b32"];
	    else if($row["type"] == "file")
		$row["url"] = "/playlist/" . $row["name_b32"] . "/play.m3u8";
	}
	
	return $rows;
    }
}

class Playlist {
    function __construct() {
	$this->api = new Api();
    }
    
    function get($filename) {
	$data = "#EXTM3U\n";
	$data .= "#EXT-X-VERSION:3\n";
	$data .= "#EXT-X-ALLOW-CACHE:YES\n";
	$rows = $this->api->request("playlist/" . $filename);
	
	if(!isset($rows["segments"]))
	    return "";
	
	$rows = $rows["segments"];
	
	if(count($rows) < 1)
	    return "";
	
	$data .= "#EXT-X-TARGETDURATION:".$rows[0]["duration"] . "\n";
	
	foreach($rows as $row) {
	    $data .= "#EXTINF:".$row["duration"].",\n";
	    $data .= $this->api->daemon_url . $row["url"] . "\n";
	}
	
	$data .= "#EXT-X-ENDLIST\n";
	
	return $data;
    }
    
    function header() {
	header("Content-Type: application/vnd.apple.mpegurl");
    }
}

class Api {
    function __construct() {
	$this->CONFIG = Config::params();
	
	$this->url = $this->CONFIG["webui"]["BACKEND_URL"];
	$this->daemon_url = $this->CONFIG["webui"]["DAEMON_URL"];
    }
    
    function request($path) {
	$url = $this->url . "/" . $path;
	$data = file_get_contents($url);
	return json_decode($data, true);
    }
}

class RequestHandler {
    private $TYPE = false;
    private $DATA = array();
    
    function __construct() {
    }
    
    function go() {
	$args = explode("/", $_SERVER["REQUEST_URI"]);
	array_shift($args);
	
	$this->TYPE = $args[0];
	
	if($args[0] == "dir") {
	    $d = new Dirs();
	    $this->DATA = $d->read($args[1]);
	}
	else if($args[0] == "playlist" ) {
	    $p = new Playlist();
	    $p->header();
	    $this->DATA = $p->get($args[1]);
	}
    }
    
    function type() {
	return $this->TYPE;
    }
    
    function data() {
	return $this->DATA;
    }
}

?>