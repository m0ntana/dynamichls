#ifndef FFMPEG_H
#define FFMPEG_H

#include <string>
#include <unistd.h>
#include <poll.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>

#include "logger.h"
#include "utils.h"
#include "ffprobe.h"

class FFMpeg
{
public:
    struct ts_chunk_t {
        char *data = NULL;
        char *wptr = NULL;
        int size = 0;
    };

    const std::string session;

    FFMpeg(std::string _session);
    ~FFMpeg();
    void set_filename_from_b32(std::string b32data);
    void set_filename(std::string data);
    bool start();
    bool complete_stop();
    bool chunk(int chunk_index, ts_chunk_t *tschunk);
    void free_chunk_data(ts_chunk_t *c);
    std::string last_error();
    bool is_expired();

    static std::string new_session();

private:
    struct ffpipe_t {
        int my_stdin, my_stdout;
        int his_stdin, his_stdout;
    };
    log4cpp::Category *log;
    std::string filename;
    ffpipe_t pipes;
    FILE* input_handle = nullptr;
    int child_pid = -1;
    int last_chunk_index = 0;
    std::string _last_error;
    FFProbe::file_info_t file_info;
    time_t expires_at = 0;


    // only for debug
    //int INPUT_CHUNK_SIZE = 2 * 1024 * 1024;

    void setup_pipes();
    bool open_input_file();
    int read_input_chunk(char *buffer, int buflen);
    void reset_input_file();
    void close_input_file();
    void convert_to_ts(char *datap, int len, ts_chunk_t *tschunk);
    bool stop();
    void reset();
    void update_expire_timer();

};

#endif // FFMPEG_H
