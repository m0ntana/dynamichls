#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include <string>
#include <fstream>

#include <nlohmann/json.hpp>

class ConfigFile
{
public:
    class _Config {
    public:
        bool ok = true;

        struct _webui {
            std::string backend_url, frontend_url;
        } webui;

        struct _daemon {
            bool debug = true;
            std::string root_dir, log_file, listen;
        } daemon;

        struct _ff {
            bool debug = false;
            std::string binary, arguments;
        } ffprobe, ffmpeg;

    } Config;

    ConfigFile(std::string filename);
    static _Config get(std::string filename);
    static _Config get();

private:
    bool _ok = true;
    std::string filename;
    static ConfigFile *instance;

    void read_config(std::string filename);
    void read_section_daemon(nlohmann::json data);
    void read_section_ff(std::string name, _Config::_ff &ff, nlohmann::json data);

    void not_ok();
};

#endif // CONFIGFILE_H
