#include "configfile.h"

ConfigFile* ConfigFile::instance = NULL;

ConfigFile::ConfigFile(std::string filename)
{
    std::cerr << "ConfigFile()\n";
    read_config(filename);
}

ConfigFile::_Config ConfigFile::get(std::string filename)
{
    if(ConfigFile::instance == NULL)
        ConfigFile::instance = new ConfigFile(filename);
    return ConfigFile::instance->Config;
}

ConfigFile::_Config ConfigFile::get()
{
    // you should not call get() before get(filename); see main.cpp
    assert(ConfigFile::instance != NULL);

    return ConfigFile::instance->Config;
}

void ConfigFile::read_config(std::string filename)
{
    nlohmann::json data;
    try {
        std::ifstream ihandle(filename);
        data = nlohmann::json::parse(ihandle);
    } catch(std::exception &e) {
        std::cerr << "can't read config file: " << filename << ": " << e.what() << std::endl;
        return not_ok();
    }

    read_section_daemon(data);
    read_section_ff("ffprobe", Config.ffprobe, data);
    read_section_ff("ffmpeg", Config.ffmpeg, data);
}

void ConfigFile::read_section_daemon(nlohmann::json data)
{
    if(data["daemon"].size() < 1) {
        std::cerr << "config: no 'daemon' section" << std::endl;
        return not_ok();
    }

    try {
        Config.daemon.listen = data["daemon"]["listen"];
    } catch(std::exception &e) {
        std::cerr << "config: no 'listen' specified: " << e.what() << std::endl;
        return not_ok();
    }

    try {
        Config.daemon.root_dir = data["daemon"]["root_dir"];
    } catch(std::exception &e) {
        std::cerr << "config: no 'root_dir' specified: " << e.what() << std::endl;
        return not_ok();
    }

    try {
        Config.daemon.log_file = data["daemon"]["log_file"];
    } catch(...) {}

    try {
        Config.daemon.debug = data["daemon"]["debug"];
    } catch(...) {;}
}

void ConfigFile::read_section_ff(std::string name, ConfigFile::_Config::_ff &ff, nlohmann::json data)
{
    if(data[name].size() < 1) {
        std::cerr << "config: no '" << name << "' section" << std::endl;
        return not_ok();
    }

    try {
        ff.binary = data[name]["binary"];
    } catch(std::exception &e) {
        std::cerr << "config: no '" << name << "->binary' specified: " << e.what() << std::endl;
        return not_ok();
    }

    try {
        ff.arguments = data[name]["arguments"];
    } catch(std::exception &e) {
        std::cerr << "config: no '" << name << "->arguments' specified: " << e.what() << std::endl;
        return not_ok();
    }

    try {
        ff.debug = data[name]["debug"];
    } catch(...) {;}

}


void ConfigFile::not_ok()
{
    Config.ok = false;
}
