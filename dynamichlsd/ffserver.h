#ifndef FFSERVER_H
#define FFSERVER_H

#include "logger.h"
#include "ffmpeg.h"
#include <map>

class FFServer
{
public:
    FFServer();
    static FFServer* instance();
    static FFMpeg *get_ffmpeg(std::vector<std::string> args);
    static void stop_all();
    static void start_thread_which_stops_idle();
private:
    std::vector<FFMpeg*> _ffmpeg_instances;
    log4cpp::Category *log;
    static FFServer* _instance;

    FFMpeg *_get_ffmpeg(std::vector<std::string> args);
    void _stop_all();
    void free_ffmpeg_instance(FFMpeg *instance);
    static void* thread_stop_idle(void *arg);
};

#endif // FFSERVER_H
