#include <iostream>
#include <stdio.h>

#define NO_FCGI_DEFINES
#include "server.h"
#undef NO_FCGI_DEFINES

#include "utils.h"
#include "configfile.h"

Server *server = NULL;

void interrupt_handler(int sig) {
    fprintf(stderr, "signal received: %d\n", sig);
    if(!server)
        fprintf(stderr, "no server actually running\n");
    else
        server->stop();
}

int main(int argc, char **argv)
{
    bool error = true;
    if(argc < 2)
        fprintf(stderr, "no config file specified\n");
    else
        error = false;

    if(error) {
        fprintf(stderr, "%s <path/config.json>\n", argv[0]);
        return 1;
    }

    ConfigFile::_Config conf = ConfigFile::get(argv[1]);
    if(!conf.ok)
        return 1;

    signal(SIGINT, interrupt_handler);



    Server s;
    server = &s;
    s.start(1024);


    return 0;
}
