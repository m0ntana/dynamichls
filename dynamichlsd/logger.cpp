#include "logger.h"

log4cpp::Category *Logger::m_logger = NULL;

Logger::Logger()
{

}

log4cpp::Category& Logger::get(std::string logfile)
{
    if(Logger::m_logger)
        return *(Logger::m_logger);

    log4cpp::PatternLayout *layout = new log4cpp::PatternLayout();
    layout->setConversionPattern("%d %p: %m %n");

    log4cpp::LayoutAppender *_stderr = NULL;
    if(logfile.size() > 0)
        _stderr = new log4cpp::FileAppender("stderr", logfile);
    else
        _stderr = new log4cpp::OstreamAppender("stderr", &std::cerr);
    _stderr->setLayout(layout);

    m_logger = &(log4cpp::Category::getRoot());
    m_logger->addAppender(_stderr);

    m_logger->setPriority(log4cpp::Priority::DEBUG);

    return *(Logger::m_logger);
}

void Logger::set_debug(bool value)
{
    if(value)
        m_logger->setPriority(log4cpp::Priority::DEBUG);
    else
        m_logger->setPriority(log4cpp::Priority::INFO);
}
