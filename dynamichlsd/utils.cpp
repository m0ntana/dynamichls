#include "utils.h"

unsigned char Utils::b32e_rfc4648[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
unsigned char Utils::b32d_rfc4648[] = {
    /*                                                Special Shit                                          */
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /*	SP !  "  #  $  %  &  '  (  )  *  +  ,  -  .  /  0  1  2  3  4  5  6  7  8  9  :  ;  <  =  >  ?  */
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,26,27,28,29,30,31, 0, 0, 0, 0, 0, 0, 0, 0,
    /*      @  A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z  [  \  ]  ^  _  */
        0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25, 0, 0, 0, 0, 0,
    /*      `  a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  {  |  }  ~     */
        0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25, 0, 0, 0, 0, 0,
    /*                                                      ‘  ’                                            */
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /*                                          Un-American Gibberish                                       */
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /*                                          Un-American Gibberish                                       */
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /*                                          Un-American Gibberish                                       */
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };
Utils::Utils()
{

}

char** Utils::split_command(std::string cmd)
{
    char **splitted;
    std::vector<std::string> args = split_string(cmd.c_str(), ' ');

    int size = (args.size() + 1) * sizeof(char*);
    splitted = (char**)malloc(size);

    for(int i = 0; i < (int)args.size(); i++) {
        char *ptr = (char*)(args[i].c_str());
        splitted[i] = new char[strlen(ptr)];
        strcpy(splitted[i], ptr);
    }
    splitted[args.size()] = NULL;

    return splitted;
}

std::vector<std::string> Utils::split_string(const char *data, char delimiter)
{
    std::vector<std::string> args;

    for(int i=0; *data != 0; data++) {
        if((int)args.size() <= i)
            args.push_back(std::string());

        if(*data == delimiter || *data == 0)
            i++;
        else
            args[i] += *data;
    }

    return args;
}

std::string Utils::base32_encode(std::string input)
{
    int out_len = input.size() * 4;
    char out[out_len];
    memset(&out, 0x0, out_len);

    Utils u;
    u.b32e((char*)input.c_str(), input.size(), out, (char*)b32e_rfc4648);
    return std::string(out);
}

std::string Utils::base32_decode(std::string input)
{
    int out_len = input.size() * 4;
    char out[out_len];
    memset(&out, 0x0, out_len);

    Utils u;
    u.b32d((char*)input.c_str(), input.size(), out, (char*)b32d_rfc4648);

    return std::string(out);
}

/*
 * chunk URL:
 * /chunk/[session_key]/[filename_base32_encoded]/[chunk_index]/data.ts
 *
 */

Utils::chunk_info_t Utils::url2chunk(std::string url)
{
    return url2chunk(Utils::split_string(url.c_str(), '/'));
}

Utils::chunk_info_t Utils::url2chunk(std::vector<std::string> args)
{
    chunk_info_t info;
    if(args[0].compare("chunk") != 0)
        return info;

    if(args.size() < 4)
        return info;

    info.session = args[1];
    info.filename_b32 = args[2];
    info.filename = Utils::base32_decode(info.filename_b32);
    info.index = std::stoi(args[3]);

    info.ready = true;

    return info;
}

std::string Utils::chunk2url(Utils::chunk_info_t info)
{
    std::string url = "/chunk/" + info.session + "/" + info.filename_b32 + "/" + std::to_string(info.index) + "/data.ts";
    return url;
}

unsigned char *Utils::b32e_5(unsigned char *in, int len, unsigned char *out, unsigned char *table)
{
    int x, y;
    uint64_t p, t=0;
    for(x=0, y=4; x < 5; ++x, --y) {
        p = in[x];
        t += p << ((+y)*8);
    }
    for(x=0, y=7; x < 8; ++x, --y) {
        out[x] = table[((t >> ((+y)*5)) & 0x1F)];
    }
    if(len<5)
        switch(len) { // why? because fuck it, thats why
            case 4: // 1
                out[7] = '=';
                break;
            case 3: // 3
                out[7] = '=';
                out[6] = '=';
                out[5] = '=';
                break;
            case 2: // 4
                out[7] = '=';
                out[6] = '=';
                out[5] = '=';
                out[4] = '=';
                break;
            case 1: // 6
                out[7] = '=';
                out[6] = '=';
                out[5] = '=';
                out[4] = '=';
                out[3] = '=';
                out[2] = '=';
                break;
            default:
                break;
        }
    return out;
}

unsigned char *Utils::b32d_8(unsigned char *in, unsigned char *out, unsigned char *table)
{
    int x, y;
    uint64_t t=0, a;
    for(x=0; x < 8; ++x)
        if(in[x] == '=')
            in[x] = 0; // convert padding to zero;
    for(x=0, y=7; x < 8; ++x, --y) {
        a = table[in[x]];
        t += (a << ((+y)*5));
    }
    for(x=0, y=4; x < 5; ++x, --y)
        out[x] = (t >> ((+y)*8));
    return out;
}

char *Utils::b32e(char *in, int len, char *out, char *table)
{
    int x, y;
    for(x=0, y=0; x < len; x+=5, y+=8)
        b32e_5((unsigned char*)(in+x), len-x, (unsigned char*)(out+y), (unsigned char*)table);
    return out;
}

char *Utils::b32d(char *in, int len, char *out, char *table)
{
    int x, y;
    for(x=0, y=0; x < len; x+=8, y+=5)
        b32d_8((unsigned char*)(in+x), (unsigned char*)(out+y), (unsigned char*)table);
    return out;
}
