#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <vector>
#include <string.h>
#include "logger.h"

class Utils
{
public:
    struct chunk_info_t {
        bool ready = false;
        std::string session, filename, filename_b32;
        unsigned int index = 0;
    };


    Utils();

    static char **split_command(std::string cmd);
    static std::vector<std::string> split_string(const char *data, char delimiter);
    static std::string base32_encode(std::string input);
    static std::string base32_decode(std::string input);

    static chunk_info_t url2chunk(std::string url);
    static chunk_info_t url2chunk(std::vector<std::string> args);
    static std::string chunk2url(chunk_info_t info);

private:

    // base32 internal functions
    // source: https://github.com/WWelna/Base32/blob/master/b32.c
    static unsigned char b32e_rfc4648[];
    static unsigned char b32d_rfc4648[];

    unsigned char *b32e_5(unsigned char *in, int len, unsigned char *out, unsigned char *table);
    unsigned char *b32d_8(unsigned char *in, unsigned char *out, unsigned char *table);
    char *b32e(char *in, int len, char *out, char *table);
    char *b32d(char *in, int len, char *out, char *table);
    // end of Base32 utilities
};

#endif // UTILS_H
