#ifndef FFPROBE_H
#define FFPROBE_H

#include <stdio.h>
#include <string>
#include <regex>
#include <math.h>

#include "utils.h"
#include "logger.h"
#include "dirhandler.h"
#include "configfile.h"

class FFProbe
{
public:
    struct file_info_t {
        double duration = -1;
        unsigned long long size = 0;
        int chunk_size = 0;
        unsigned int num_chunks = 0;
    };
    FFProbe();
    file_info_t info_b32(std::string filename_b32);
    file_info_t info(std::string filename);
private:
    FILE *handle = NULL;
    log4cpp::Category *log;
    std::string filename;
};

#endif // FFPROBE_H
