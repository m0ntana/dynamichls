#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H

#include <fcgi_config.h>
#include <fcgi_stdio.h>

#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include <string>
#include <nlohmann/json.hpp>


#include "logger.h"
#include "ffserver.h"
#include "utils.h"
#include "dirhandler.h"

class RequestHandler
{
public:
    static pthread_mutex_t accept_mutex;

    RequestHandler();
    static void threaded(void *arg);
    void handle(int socket);

private:
    log4cpp::Category *log;
    bool _headers_sent = false;
    FCGX_Request request;
    std::string _request_uri, _request_method;

    static void *threaded_handler(void *arg);

    void debug_request_params();

    void set_content_type(std::string type);
    void send_header(std::string name, std::string value);
    void end_headers();

    void bytes_answer(const char *data, int len);
    void answer(const char *data);
    void answer(std::string data);

    void handle_chunk(std::vector<std::string> args);
    void handle_playlist(std::vector<std::string> args);
    void handle_dir(std::vector<std::string> args);

    std::vector<std::string> get_arguments();

};

#endif // REQUESTHANDLER_H
