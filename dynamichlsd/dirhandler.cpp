#include "dirhandler.h"

//std::string DirHandler::root_dir;

DirHandler* DirHandler::_instance = NULL;

DirHandler::DirHandler()
{
    log = &(Logger::get());
    log->debugStream() << "DirHandler()";
    config = ConfigFile::get();
    root_dir = config.daemon.root_dir;
}

nlohmann::json DirHandler::handle_request(std::string _dir)
{
    nlohmann::json records;

    if(root_dir.size() < 1) {
        log->errorStream() << "no root dir specified";
        return records;
    }

    std::string fullpath = root_dir + "/" + _dir;

    if(!is_in_root(fullpath)) {
        log->errorStream() << "dir request: " << fullpath << ": outside of the root directory";
        return records;
    }

    DIR* dir = opendir(fullpath.c_str());
    struct dirent *entry;
    if(dir == nullptr) {
        log->errorStream() << "can't open dir: " << fullpath << strerror(errno);
        return records;
    }

    nlohmann::json updir;
    updir["name"] = "..";
    updir["name_b32"] = Utils::base32_encode(_dir + "/..");
    updir["type"] = "dir";
    records.push_back(updir);


    while((entry = readdir(dir)) != NULL) {
        nlohmann::json record;

        record["name"] = std::string(entry->d_name);
        record["name_b32"] = Utils::base32_encode(_dir + "/" + record["name"].get<std::string>());

        if(record["name"].get<std::string>().compare(".") == 0 ||
           record["name"].get<std::string>().compare("..") == 0)
            continue;

        if(entry->d_type == DT_DIR) {
            record["type"]="dir";
            //record["url"] = "/dir/" + Utils::base32_encode(record["name"]);
        }
        else if(entry->d_type == DT_REG) {
            record["type"]="file";
            //record["url"] = "/playlist/" + Utils::base32_encode(record["name"]) + "/play.m3u8";
        }
        else
            continue;



        records.push_back(record);
    }
    closedir(dir);

    return records;
}

std::string DirHandler::get_full_name(std::string filename)
{
    return DirHandler::instance()->root_dir + "/" + filename;
}

DirHandler *DirHandler::instance()
{
    if(DirHandler::_instance == NULL)
        DirHandler::_instance = new DirHandler();

    return DirHandler::_instance;
}

bool DirHandler::is_in_root(std::string path)
{
    char real[PATH_MAX];
    realpath(path.c_str(), real);

    return (std::string(real).find(root_dir) != std::string::npos);

//    log->debugStream() << "resolved path: " << real;

//    return true;
}
