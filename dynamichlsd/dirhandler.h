#ifndef DIRHANDLER_H
#define DIRHANDLER_H

#include <string>
#include <sys/types.h>
#include <dirent.h>
#include <nlohmann/json.hpp>

#include "logger.h"
#include "utils.h"
#include "configfile.h"

class DirHandler
{
public:
    struct dir_entry_t {
        std::string name, type, url;
    };

    DirHandler();
    nlohmann::json handle_request(std::string _dir);
    static std::string get_full_name(std::string filename);

private:
    log4cpp::Category *log;
    ConfigFile::_Config config;
    std::string root_dir;
    static DirHandler *_instance;

    static DirHandler* instance();
    bool is_in_root(std::string path);
};

#endif // DIRHANDLER_H
