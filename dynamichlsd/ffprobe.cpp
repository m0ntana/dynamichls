#include "ffprobe.h"

FFProbe::FFProbe()
{
    log = &(Logger::get());
}

FFProbe::file_info_t FFProbe::info_b32(std::string filename_b32)
{
    return info(Utils::base32_decode(filename_b32));
}

FFProbe::file_info_t FFProbe::info(std::string _filename)
{
    filename = _filename;

    std::string cmd = ConfigFile::get().ffprobe.binary + " " + ConfigFile::get().ffprobe.arguments;
    cmd = std::regex_replace(cmd, std::regex("_FILENAME_"), DirHandler::get_full_name(filename));

    file_info_t info;

    log->debugStream() << "ffprobe:\n" << cmd;

    if((handle = popen(cmd.c_str(), "r")) == nullptr) {
        log->errorStream() << "can't start process";
        return info;
    }

    int buflen = 16 * 1024;
    char buf[buflen];

    int bread = fread(&buf, 1, buflen, handle);
    log->debugStream() << "output:";
    log->debugStream() << buf;

    if(bread > 0) {
        std::vector<std::string> args = Utils::split_string(buf, '|');

        try {
            info.duration = std::stod(args[7]);
            info.size = std::stoull(args[8]);
        } catch(std::exception &e) {
            log->debugStream() << "can't parse: '" << args[7] << "' or '" << args[8] << "'";
        }

        info.chunk_size = ceil(double(info.size) / info.duration * 10);
        info.num_chunks = ceil(double(info.size) / double(info.chunk_size));

        log->debugStream() << "chunk size: " << info.chunk_size << "; num chunks: " << info.num_chunks;
    }

    pclose(handle);

    return info;
}
