#include "ffserver.h"

FFServer* FFServer::_instance = nullptr;

FFServer::FFServer()
{
    log = &(Logger::get());

    log->debugStream() << "FFServer()";
}

FFMpeg* FFServer::get_ffmpeg(std::vector<std::string> args)
{
    FFServer *instance = FFServer::instance();

    return instance->_get_ffmpeg(args);
}

void FFServer::stop_all()
{
    FFServer::instance()->_stop_all();
}

void FFServer::start_thread_which_stops_idle()
{
    FFServer *h = FFServer::instance();
    pthread_t t;
    pthread_create(&t, NULL, &FFServer::thread_stop_idle, h);
}

FFServer *FFServer::instance()
{
    if(FFServer::_instance == nullptr)
        FFServer::_instance = new FFServer();

    return FFServer::_instance;
}

FFMpeg* FFServer::_get_ffmpeg(std::vector<std::string> args)
{
    FFMpeg *f = nullptr;

    Utils::chunk_info_t info = Utils::url2chunk(args);

    for(FFMpeg *h : _ffmpeg_instances) {
        if(h->session.compare(info.session) == 0) {
            f = h;
            break;
        }
    }

    if(f == nullptr) {
        f = new FFMpeg(info.session);
        f->set_filename(info.filename);
        if(!f->start()) {
            delete f;
            return nullptr;
        }
        _ffmpeg_instances.push_back(f);
        log->debugStream() << "new FFMpeg instance created for session: " << f->session;
    }
    return f;

}

void FFServer::_stop_all()
{
    int i = 0;
    for(FFMpeg *h : _ffmpeg_instances) {
        h->complete_stop();
        i++;
    }
}

void FFServer::free_ffmpeg_instance(FFMpeg *instance)
{
    _ffmpeg_instances.erase(std::remove(_ffmpeg_instances.begin(), _ffmpeg_instances.end(), instance), _ffmpeg_instances.end());
    delete instance;
}

void *FFServer::thread_stop_idle(void *arg)
{
    FFServer *h = (FFServer*)arg;
    while(true) {
        for(FFMpeg *ffmpeg : h->_ffmpeg_instances) {
            if(!ffmpeg->is_expired())
                continue;
            ffmpeg->complete_stop();
            h->free_ffmpeg_instance(ffmpeg);
        }
        sleep(1);
    }
}
