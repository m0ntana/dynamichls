#include "ffmpeg.h"

FFMpeg::FFMpeg(std::string _session) : session(_session)
{
    log = &(Logger::get());
}

FFMpeg::~FFMpeg()
{
    log->debugStream() << "~FFMpeg()";
}

void FFMpeg::set_filename_from_b32(std::string b32data)
{
    this->set_filename(Utils::base32_decode(b32data));
    log->debugStream() << "ffmpeg filename: '" << filename << "'";
}

void FFMpeg::set_filename(std::string data)
{
    this->filename = data;
}

bool FFMpeg::start()
{
    ConfigFile::_Config config = ConfigFile::get();
    char **cmd = Utils::split_command(config.ffmpeg.binary + " " + config.ffmpeg.arguments);

    if(!open_input_file()) {
        log->errorStream() << "can't open file for reading: " << filename;
        return false;
    }

    if(file_info.duration >= 0)
        log->debugStream() << "already known file: " << filename;
    else {
        FFProbe ffp;
        file_info = ffp.info(filename);
    }

    setup_pipes();
    update_expire_timer();

    int pid = fork();

    if(pid == 0) {
        // we are child
        close(0);
        close(1);
        if(!config.ffmpeg.debug)
            close(2);

        dup2(pipes.his_stdin, 0);
        dup2(pipes.his_stdout, 1);


        int ret = execv(cmd[0], cmd);
        if(ret < 0) {
            log->errorStream() << "forked; can't start " << cmd[0];
            _exit(1);
        }
    }
    else if(pid > 0) {
        child_pid = pid;
        signal(SIGCHLD, SIG_IGN);
        log->debugStream() << "forked; PID: " << pid;
    }
    else {
        log->errorStream() << "error forking";
    }
    return true;
}

bool FFMpeg::complete_stop()
{
    close_input_file();
    stop();

    return true;
}

bool FFMpeg::chunk(int chunk_index, FFMpeg::ts_chunk_t *tschunk)
{
    char ichunk[file_info.chunk_size];

    if(chunk_index < 1) {
        _last_error = std::string("invalid chunk index " + chunk_index);
        return false;
    }

    if(chunk_index <= last_chunk_index)
        reset();

    for(int i = last_chunk_index; i < chunk_index; i++) {
        int rsize = read_input_chunk(ichunk, file_info.chunk_size);
        log->debugStream() << "input data len: " << rsize;
        if(rsize == 0 && feof(input_handle)) {
            log->warnStream() << "ffmpeg[" << session << "]: EOF reached; stop";
            stop();
            return true;
        }

        ts_chunk_t *c = NULL;
        if(i == (chunk_index-1))
            c = tschunk;
        else
            log->debugStream() << "skip chunk " << (i+1);

        convert_to_ts(ichunk, rsize, c);
        update_expire_timer();
    }

    last_chunk_index = chunk_index;

    return true;
}

void FFMpeg::setup_pipes()
{
    int fds[2];

    pipe(fds);
    pipes.my_stdout=fds[1];
    pipes.his_stdin=fds[0];

    pipe(fds);
    pipes.my_stdin = fds[0];
    pipes.his_stdout = fds[1];

    int flags = fcntl(pipes.my_stdout, F_GETFL, 0);
    fcntl(pipes.my_stdout, F_SETFL, flags | O_NONBLOCK);
}

bool FFMpeg::open_input_file()
{
    if(input_handle != nullptr)
        return true;

    if((input_handle = fopen(DirHandler::get_full_name(filename).c_str(), "rb")) == nullptr) {
        log->errorStream() << "can't open input file: " << filename;
        return false;
    }

    return true;
}

int FFMpeg::read_input_chunk(char *buffer, int buflen)
{
    int bread = fread(buffer, 1, buflen, input_handle);

    return bread;
}

void FFMpeg::reset_input_file()
{
    if(input_handle != nullptr)
        rewind(input_handle);
}

void FFMpeg::close_input_file()
{
    if(input_handle == nullptr)
        return;
    fclose(input_handle);
    input_handle = nullptr;
}

void FFMpeg::convert_to_ts(char *datap, int len, ts_chunk_t *tschunk)
{
    char *end_p = datap + len;
    struct pollfd fds[2];

    fds[0].fd = pipes.my_stdin;
    fds[0].events = POLLIN;

    fds[1].fd = pipes.my_stdout;
    fds[1].events = POLLOUT;

    free_chunk_data(tschunk);

    int max_write_size = 128 * 1024;
    int total_written = 0;

    while(datap < end_p) {
        int ret = poll(fds, 2, 1000);
        if(ret == 0) {
            log->debugStream() << "poll() timed out";
            continue;
        }

        if(fds[1].revents & POLLOUT) {
            int data_size = ((end_p - datap) < max_write_size)? end_p - datap : max_write_size;
            if(data_size <= 0)
                break;
            int written = write(pipes.my_stdout, datap, data_size);
            datap += written;
            total_written += written;
        }

        if(fds[0].revents & POLLIN) {
            int buflen = 2 * file_info.chunk_size;
            char buf[buflen];
            int bread = read(pipes.my_stdin, &buf, buflen);

            //log->debugStream() << "read TS: " << bread;
            if(bread == 0)
                break;

            if(tschunk == NULL)
                continue;

            if(tschunk->data == NULL) {
                //log->debugStream() << "initialize chunk data: " << bread;
                tschunk->data = (char*)malloc(bread);
            }
            else {
                tschunk->data = (char*)realloc(tschunk->data, tschunk->size + bread);
                assert(tschunk->data != NULL);
                //log->debugStream() << "realloc to: " << tschunk->size + bread;
            }



            tschunk->wptr = tschunk->data + tschunk->size;
            assert(tschunk->wptr != NULL);

            memcpy(tschunk->wptr, &buf, bread);

            tschunk->size += bread;
        }
    }
    log->debugStream() << "total written to ffmpeg: " << total_written;
}

bool FFMpeg::stop()
{
    if(child_pid < 0) {
        log->errorStream() << "we don't have child";
        return false;
    }

    log->debugStream() << "stop PID " << child_pid;
    log->debugStream() << "close FDs";

    close(pipes.his_stdin);
    close(pipes.his_stdout);
    close(pipes.my_stdin);
    close(pipes.my_stdout);

    log->debugStream() << "kill PID " << child_pid;
    kill(child_pid, SIGKILL);

    log->infoStream() << "ffmpeg[" << child_pid << "] stopped";

    return true;
}

void FFMpeg::reset()
{
    log->infoStream() << "ffmpeg[" << session << "]: reset";
    reset_input_file();
    stop();
    start();
    last_chunk_index = 0;
}

void FFMpeg::update_expire_timer()
{
    expires_at = time(NULL) + 30;
}

void FFMpeg::free_chunk_data(ts_chunk_t *c)
{
    if(c == NULL || c->data == NULL)
        return;

    log->debugStream() << "free chunk data";

    free(c->data);
    c->data = NULL;
    c->size = 0;
}

std::string FFMpeg::last_error()
{
    return _last_error;
}

bool FFMpeg::is_expired()
{
    return (expires_at > 0 && (time(NULL) > expires_at));
}

std::string FFMpeg::new_session()
{
    srand(time(NULL));
    int val = rand() % 10000;

    return std::to_string(val);
}
