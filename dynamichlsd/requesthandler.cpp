#include "requesthandler.h"

pthread_mutex_t RequestHandler::accept_mutex = PTHREAD_MUTEX_INITIALIZER;

RequestHandler::RequestHandler()
{
    log = &(Logger::get());
}

void RequestHandler::threaded(void *arg)
{
    pthread_t t;
    pthread_create(&t, NULL, threaded_handler, arg);
}

void *RequestHandler::threaded_handler(void *arg)
{
    int s = *((int*)arg);
    RequestHandler rhandler;
    rhandler.handle(s);

    return NULL;
}

void RequestHandler::debug_request_params()
{
    std::string sdebug;

    for(int i=0; ; i++) {
        char *p = request.envp[i];
        if(p != NULL)
            sdebug += std::string(p) + "\n";
        else
            break;
    }
    log->debugStream() << "input FCGI parameters:\n" << sdebug;
}

void RequestHandler::handle(int socket)
{
    FCGX_InitRequest(&request, socket, 0);

    FCGX_Accept_r(&request);

    pthread_mutex_unlock(&(RequestHandler::accept_mutex));



    std::vector<std::string> args = get_arguments();

    log->infoStream() << _request_method << " " << _request_uri;
    debug_request_params();

    if(args[0].compare("chunk") == 0)
        handle_chunk(args);
    else if(args[0].compare("playlist") == 0)
        handle_playlist(args);
    else if(args[0].compare("dir") == 0)
        handle_dir(args);


    FCGX_Finish_r(&request);
    FCGX_Free(&request, 1);

    log->debugStream() << "request handled";
}

void RequestHandler::set_content_type(std::string type)
{
    send_header("Content-Type", type);
}

void RequestHandler::send_header(std::string name, std::string value)
{
    FCGX_FPrintF(request.out, "%s: %s\r\n", name.c_str(), value.c_str());
}

void RequestHandler::end_headers()
{
    FCGX_FPrintF(request.out, "\r\n");
}

void RequestHandler::bytes_answer(const char *data, int len)
{
    FCGX_PutStr(data, len, request.out);
}

void RequestHandler::answer(const char *data)
{
    bytes_answer(data, strlen(data));
}

void RequestHandler::answer(std::string data)
{
    answer(data.c_str());
}

void RequestHandler::handle_chunk(std::vector<std::string> args)
{
    if(args.size() < 4) {
        set_content_type((char*)"plain/text");
        end_headers();

        answer("not enough parameters\n");
        return;
    }

    Utils::chunk_info_t info = Utils::url2chunk(args);
    if(!info.ready) {
        set_content_type((char*)"plain/text");
        end_headers();

        answer("can't parse chunk URL\n");
        return;
    }

    FFMpeg *f = FFServer::get_ffmpeg(args);
    if(!f) {
        set_content_type((char*)"plain/text");
        end_headers();

        answer("error on file\n");
        return;
    }

    FFMpeg::ts_chunk_t tschunk;
    if(!f->chunk(info.index, &tschunk)) {
        set_content_type((char*)"plain/text");
        end_headers();
        answer(f->last_error());
        return;
    }

    assert(tschunk.data != NULL);

    log->debugStream() << "TS data len: " << tschunk.size;

    set_content_type((char*)"video/mp2t");
    end_headers();

    bytes_answer(tschunk.data, tschunk.size);

    f->free_chunk_data(&tschunk);
}

void RequestHandler::handle_playlist(std::vector<std::string> args)
{
    nlohmann::json list;
    list["segments"] = {};


    FFProbe f;
    FFProbe::file_info_t info = f.info_b32(args[1]);

    Utils::chunk_info_t chunk_info;
    chunk_info.session = FFMpeg::new_session();
    chunk_info.filename_b32 = args[1];

    for(chunk_info.index=1; chunk_info.index <= info.num_chunks; chunk_info.index++) {
        nlohmann::json segment;
        segment["duration"] = 10;
        segment["url"] = Utils::chunk2url(chunk_info);
        list["segments"].push_back(segment);
    }

    set_content_type((char*)"application/json");
    end_headers();

    answer(list.dump());
}

void RequestHandler::handle_dir(std::vector<std::string> args)
{
    DirHandler d;
    std::string path;
    if(args.size() > 1)
        path = Utils::base32_decode(args[1]);
    nlohmann::json data = d.handle_request(path);

    set_content_type((char*)"application/json");
    end_headers();

    answer(data.dump().c_str());
}

std::vector<std::string> RequestHandler::get_arguments()
{
    _request_method = std::string(FCGX_GetParam("REQUEST_METHOD", request.envp));
    // get URI and remove first '/' symbol
    char *data = FCGX_GetParam("REQUEST_URI", request.envp);
    _request_uri = std::string(data);
    data ++;


    return Utils::split_string(data, '/');
}
