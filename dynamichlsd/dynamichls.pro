TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        configfile.cpp \
        dirhandler.cpp \
        ffmpeg.cpp \
        ffprobe.cpp \
        ffserver.cpp \
        logger.cpp \
        main.cpp \
        requesthandler.cpp \
        server.cpp \
        utils.cpp

HEADERS += \
    configfile.h \
    dirhandler.h \
    ffmpeg.h \
    ffprobe.h \
    ffserver.h \
    logger.h \
    requesthandler.h \
    server.h \
    utils.h

LIBS += -lfcgi -llog4cpp -pthread
