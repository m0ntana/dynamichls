#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>

#include <string.h>
#include <unistd.h>
#include <fcgi_config.h>
#include <fcgi_stdio.h>
#include <dirhandler.h>

#include <fstream>

#include "logger.h"
#include "requesthandler.h"
#include "configfile.h"


class Server
{
public:
    Server();
    void start(int backlog);
    void set_root_dir(std::string dir);
    void on_terminate(int sig);
    void stop();


private:
    int _socket = 0;
    int _my_port = 0;
    log4cpp::Category *log;
    bool _running = true;
    ConfigFile::_Config config;

    void debug_request_params(FCGX_Request &request);
//    bool read_config_file(std::string filename);
//    bool parse_config_file();



    //void request_handler(FCGX_Request &request);
};

#endif // SERVER_H
