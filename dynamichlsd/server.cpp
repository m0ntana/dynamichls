#include "server.h"

Server::Server()
{
    config = ConfigFile::get();
    log = &(Logger::get(config.daemon.log_file));
    Logger::set_debug(config.daemon.debug);
}

void Server::start(int backlog)
{
    if(!_running)
        return;

    FCGX_Request request;
    FCGX_Init();
    memset(&request, 0, sizeof(FCGX_Request));

    _socket = FCGX_OpenSocket(config.daemon.listen.c_str(), backlog);

    log->infoStream() << "FCGI TCP: " << config.daemon.listen;
    log->infoStream() << "FCGI root dir: " << config.daemon.root_dir;

    FFServer::instance();

    FFServer::start_thread_which_stops_idle();

    while(true) {
        pthread_mutex_lock(&(RequestHandler::accept_mutex));
        if(!_running)
            break;
        log->debugStream() << "wait for next connection";
        RequestHandler::threaded((void*)&_socket);
    }

    log->infoStream() << "stop ffmpeg instances";
    FFServer::instance()->stop_all();

    log->infoStream() << "free resources and exit";
    FCGX_Free(&request, _socket);
}

void Server::stop()
{
    _running = false;
    pthread_mutex_unlock(&(RequestHandler::accept_mutex));
}

void Server::debug_request_params(FCGX_Request &request)
{
    for(int i=0; ; i++) {
        char *p = request.envp[i];
        if(p != NULL)
            log->debugStream() << "param: " << p;
        else
            break;
    }
}

